#!/bin/bash
# SPDX-License-Identifier: CC0-1.0
# SPDX-FileCopyrightText: 2024 Helmholtz-Zentrum hereon GmbH
# SPDX-FileContributor: Carsten Lemmen <carsten.lemmen@hereon.de>
#

ncks -O -d time,5,7 -d getmGrid2D_1,10,12 -d getmGrid3D_1,10,12 \
  -d getmGrid2D_2,10,12 -d getmGrid3D_2,10,12 -d getmGrid3D_3,17,19 \
  -d pelagic_horizontal_grid_1,10,12 -d pelagic_horizontal_grid_2,10,12 \
  -d ungridded00015,0,2 $1 $2
