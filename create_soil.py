#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# SPDX-License-Identifier: CC0-1.0
# SPDX-FileCopyrightText: 2024 Helmholtz-Zentrum hereon GmbH
# SPDX-FileContributor: Carsten Lemmen <carsten.lemmen@hereon.de>

import numpy as np
import netCDF4 as nc


def main(knum, dzmin):
    self_fac = 0.18 / ((knum + 1) / 2.0 * dzmin) - 1.0

    zi = np.zeros(knum + 1)
    dz = np.zeros(knum)
    zc = np.zeros(knum)

    for k in range(knum):
        dz[k] = (1.0 + (self_fac - 1.0) * (k + 1) / (knum - 1)) * dzmin
        zc[k] = zi[k] + 0.5 * dz[k]
        zi[k + 1] = zi[k] + dz[k]

    nc_filename = "soil.nc"
    nc_file = nc.Dataset(nc_filename, "w", format="NETCDF4")

    nc_file.createDimension("ungridded00015", knum)
    nc_file.createDimension("ungridded00016", knum + 1)

    zi_var = nc_file.createVariable(
        "level_interface_depth_in_soil", "f4", ("ungridded00016",)
    )
    zi_var.units = "m"
    zi_var.standard_name = "depth_below_sea_floor"
    zi_var.long_name = "Model level interface depth"
    zi_var.axis = "Z"

    zc_var = nc_file.createVariable(
        "level_center_depth_in_soil", "f4", ("ungridded00015",)
    )
    zc_var.units = "m"
    zc_var.standard_name = "depth_below_sea_floor"
    zc_var.long_name = "Model level center depth"
    zc_var.axis = "Z"

    dz_var = nc_file.createVariable("level_height_in_soil", "f4", ("ungridded00015",))
    dz_var.units = "m"
    dz_var.standard_name = "cell_thickness"
    dz_var.long_name = "Model level thickness"
    dz_var.coordinates = "level_center_depth_in_soil"

    zi_var[:] = zi
    zc_var[:] = zc
    dz_var[:] = dz

    nc_file.close()


if __name__ == "__main__":
    knum = 15
    dzmin = 0.004

    main(knum, dzmin)
