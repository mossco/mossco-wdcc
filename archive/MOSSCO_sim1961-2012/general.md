<!--
SPDX-License-Identifier: CC0-1.0
SPDX-FileCopyrightText: 2024 Helmholtz-Zentrum hereon GmbH
SPDX-FileContributor: Carsten Lemmen <carsten.lemmen@hereon.de>
-->

# MOSSCO Southern North Sea Long-term bentho-pelagic ecosystem simulation 1961-2012

Wirtz, Kai W. et al.

## Keywords

Bentho-pelagic coupling, GETM, MAECS, Modular coupling, MOSSCO, North Sea, OMExDia, Regional simulation

## Summary

Regional coupled ecosystem simulation of the Southern North Sea with the fully coupled Modular System for Shelves and Coasts (MOSSCO), an application layer of the Earth System Modeling Framework (ESMF). Here, we couple (1) the General Estuarine Transport Model (GETM) hydrodynamics and local waves with (2) the Model for Adaptive ECoSystems (MAECS) in the pelagic through the Framework for Aquatic Biogeochemical Models (FABM), and (3) the Ocean Margin Experiment Diagenesis (OMExDia) with added phosphorous cycle in the benthic through FABM.

Forcing and boundary conditions are provided by (1) zero-gradient open boundary dissolved and particulate nutrients from a North Atlantic shelf simulation with the Ecosystem Model Hamburg (ECOHAM), (2) astronomical forcing of tides as boundary sea surface elevation, (3) surface winds from the CoastDat2 Climate Limited Area (CLM) hindcast, (4) sediment porosity from the North Sea Observation and Assessment of Habitats (NOAH) atlas, and (5) river fluxes and nutrient loads from the Hereon River database.

The simulation covers the period 1 Feb 1961 to 31 Jan 2013. It is performed on a curvilinear grid of the Southern North Sea, represented by a 98 x 139 logically rectangular grid, with varying spatial resolution of 3.7-66 sqkm per grid cell, and highest resolution in the Elbe Estuary. Vertical resolution is 20 layers in the pelagic on terrain-following sigma coordinates, and 15 z-levels resolving the ocean floor down to 20 cm. The output format is netCDF in the Climate and Forecast (CF) convention as much as possible. Complete three-dimensional data are available at 36-hour intervals.

The coupled model system and the model setup are described in detail in Lemmen et al. (2018). Validations of the ecosystem coupling were performed, amongst others by Wirtz (2019, also describing the ecosystem model) and Slavik et al. (2019) using the same setup. Coupling to sediment processes is described by Nasermoaddeli et al. (2018) and to bentho-pelagic filtration by Lemmen (2018). The results specific to this long-term simulation have already been used by Xu et al. (2022). The model system and all of its components are available as free and open source and available from https://codebase.helmholtz.cloud/mossco/code.

- Project: MOSSCO (Modular System for Shelves and Coasts)
- Contact: Dr. Carsten Lemmen ( carsten.lemmen@hereon.de 0000-0003-3483-6036)
- Location(s): Southern North Sea
- Spatial Coverage: Longitude -1.1 to 8.76 Latitude 51 to 55.64
- Temporal Coverage: 1960-02-01 to 2013-01-31 (calendrical)

## Citation

Wirtz, Kai W.; Hofmeister, Richard; Klingbeil, Knut; Lemmen, Carsten (2024). MOSSCO Southern North Sea Long-term bentho-pelagic ecosystem simulation 1961-2012. World Data Center for Climate (WDCC) at DKRZ. https://www.wdc-climate.de/ui/entry?acronym=MOSSCO_sim1961-2012
