<!--
SPDX-License-Identifier: CC0-1.0
SPDX-FileCopyrightText: 2024 Helmholtz-Zentrum hereon GmbH
SPDX-FileContributor: Carsten Lemmen <carsten.lemmen@hereon.de>
-->

Contact type Person ORCID Institute
Contact Dr. Carsten Lemmen 0000-0003-3483-6036 Helmholtz-Zentrum Hereon
Author Prof. Kai W Wirtz 0000-0002-6972-3878 Helmholtz-Zentrum Hereon
Author Dr. Richard Hofmeister - Helmholtz-Zentrum Hereon
Author Dr. Knut Klingbeil 0000-0002-6736-1260 Leibniz-Institut für Ostseeforschung Warnemünde
Author Dr. Carsten Lemmen 0000-0003-3483-6036 Helmholtz-Zentrum Hereon
Investigator Prof. Kai W Wirtz 0000-0002-6972-3878 Helmholtz-Zentrum Hereon
Funder - - Federal Ministry of Education and Research
