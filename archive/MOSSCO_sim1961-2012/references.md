<!--
SPDX-License-Identifier: CC0-1.0
SPDX-FileCopyrightText: 2024 Helmholtz-Zentrum hereon GmbH
SPDX-FileContributor: Carsten Lemmen <carsten.lemmen@hereon.de>
-->

# References

## Is documented by

[1] DOI Slavik, Kaela; Lemmen, Carsten; Zhang, Wenyan; Kerimoglu, Onur; Klingbeil, Knut; Wirtz, Kai W. (2018). The large-scale impact of offshore wind farm structures on pelagic primary productivity in the southern North Sea. doi:10.1007/s10750-018-3653-5

[2] DOI Wirtz, Kai W. (2019). Physics or biology? Persistent chlorophyll accumulation in a shallow coastal sea explained by pathogens and carnivorous grazing. doi:10.1371/journal.pone.0212143

[3] DOI Lemmen, Carsten. (2018). North Sea Ecosystem-Scale Model-Based Quantification of Net Primary Productivity Changes by the Benthic Filter Feeder Mytilus edulis. doi:10.3390/w10111527

[4] DOI Nasermoaddeli, M.H.; Lemmen, C.; Stigge, G.; Kerimoglu, O.; Burchard, H.; Klingbeil, K.; Hofmeister, R.; Kreus, M.; Wirtz, K.W.; Kösters, F. (2018). A model study on the large-scale effect of macrofauna on the suspended sediment concentration in a shallow shelf sea. doi:10.1016/j.ecss.2017.11.002

[5] DOI Xu, Xu; Lemmen, Carsten; Wirtz, Kai W. (2020). Less Nutrients but More Phytoplankton: Long-Term Ecosystem Dynamics of the Southern North Sea. doi:10.3389/fmars.2020.00662

## Is compiled by

[1] DOI Lemmen, Carsten; Hofmeister, Richard; Klingbeil, Knut; Nasermoaddeli, M. Hassan; Kerimoglu, Onur; Burchard, Hans; Kösters, Frank; Wirtz, Kai W. (2018). Modular System for Shelves and Coasts (MOSSCO v1.0) – a flexible and multi-component framework for coupled coastal ocean ecosystem modelling. doi:10.5194/gmd-11-915-2018

[2] DOI Lemmen, Carsten; Klingbeil, Knut; Hofmeister, Richard. (2018). MOSSCO 1.0.2 "decadal reference run" (MOSSCO_1_0_2). doi:10.5281/zenodo.2527238

[3] DOI Lemmen, Carsten. (2020). platipodium/sns_1960-2014: Public configuration files as as supplement to Xu et al. 2020 (1.01). doi:10.5281/zenodo.3688217
