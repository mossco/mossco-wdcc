#!/bin/bash
# SPDX-License-Identifier: CC0-1.0
# SPDX-FileCopyrightText: 2024 Helmholtz-Zentrum hereon GmbH
# SPDX-FileContributor: Carsten Lemmen <carsten.lemmen@hereon.de>
#
cat << EOT > meta.cdl
netcdf meta {
dimensions:
variables:
// global attributes:
    :Conventions = "CF-1.11" ;
    :title = "MOSSCO Southern North Sea coastal gradient bentho-pelagic ecosystem simulation 2000-2012" ;
    :source = "MOSSCO coupled simulation" ;
    :project = "Modular System for Shelves and Coasts (MOSSCO)" ;
    :description = "Regional coupled ecosystem simulation of the Southern North Sea with the fully coupled Modular System for Shelves and Coasts (MOSSCO), an application layer of the Earth System Modeling Framework (ESMF). Here, we couple (1) the General Estuarine Transport Model (GETM) hydrodynamics and local waves with (2) the Model for Adaptive ECoSystems (MAECS) in the pelagic through the Framework for Aquatic Biogeochemical Models (FABM), and (3) the Ocean Margin Experiment Diagenesis (OMExDia) with added phosphorus cycle in the benthic through FABM. This experiment considers the increase of chlorophyll-a from the shelf towards the coast as a biologically enhanced gradient. It builds on the capability of MAECS (Wirtz and Kerimoglu 2016) to resolve adaptive traits; it tests the trophic effect of carnivory – grazing on zooplankton by juvenile fish and benthic filter feeders – and the effect of viral control on shaping this gradient by performing three sub-experiments (1) a reference including both coastal carnivory and viral control (2) a simulation with uniform carnivory, but including viral control (3) a simulation including a carnivory gradient but no viral control. This experiment is described by and evaluated by Wirtz (2019).  Forcing and boundary conditions are provided by (1) zero-gradient open boundary dissolved and particulate nutrients from a North Atlantic shelf simulation with the Ecosystem Model Hamburg (ECOHAM), (2) astronomical forcing of tides as boundary sea surface elevation, (3) surface winds from the CoastDat2 Climate Limited Area (CLM) hindcast, (4) sediment porosity from the North Sea Observation and Assessment of Habitats (NOAH) atlas, and (5) river fluxes and nutrient loads from the Hereon River database.  The simulation covers the period 1 Jan 2000 to 31 Dec 2012. It is performed on a curvilinear grid of the Southern North Sea, represented by a 98 x 139 logically rectangular grid, with varying spatial resolution of 3.7-66 sqkm per grid cell, and highest resolution in the Elbe Estuary. Vertical resolution is 20 layers in the pelagic on terrain-following sigma coordinates, and 15 z-levels resolving the ocean floor down to 20 cm. The output format is netCDF in the Climate and Forecast (CF) convention as much as possible. Complete three-dimensional data are available at 36-hour intervals. The experiment was extensively validated with observational data, including the spatially resolved ESA CCI surface chlorophyll product, the time series stations data for dissolved nutrients and chlorophyll from the Noordwijk and Terschelling transects, the stations at List, Norderney and Norderelbe, zooplankton time series data from Helgoland Roads as well as climatological zooplankton from the Continuous Plankton Recorder. The coupled model system and the model setup are described in detail in Lemmen et al. (2018). Validations of the ecosystem coupling were performed, amongst others Slavik et al. (2019) using the same setup. Coupling to sediment processes is described by Nasermoaddeli et al. (2018) and to bentho-pelagic filtration by Lemmen (2018). Model physics has been evaluated by Xu et al. (2022). The model system and all of its components are available as free and open source and available from https://codebase.helmholtz.cloud/mossco/code." ;
    :experiment_id = "MOSSCO_grad2000-2012";
    :source_code_git_sha = "b10269cdc89efce149b293863496d5e7315696be";
    :source_code_git_repository = "https://codebase.helmholtz.cloud/mossco/code";
    :creation_date = "2017-09-13T11:30:00";
    :modification_date = "2024-08-27T16:00:00";
    :institution = "MOSSCO partners (Hereon, IOW, and BAW)" ;
    :institution_hereon = "Helmholtz-Zentrum hereon GmbH" ;
    :institution_iow = "Leibniz-Institut für Ostseeforschung Warnemünde" ;
    :institution_baw = "Bundesanstalt für Wasserbau" ;
    :institute_hereon_id = "https://ror.org/03qjp1d79";
    :institute_iow_id = "https://ror.org/03xh9nq73";
    :institute_baw_id = "https://ror.org/03z6hnk02";
    :references = "Wirtz (2019) PLoS One" ;
    :contact = "http://www.mossco.de/doc" ;
    :originator = "kai.wirtz@hereon.de" ;
    :creator = "carsten.lemmen@hereon.de" ;
    :crs = "EPSG:4326" ;
    :StartTime = "2000-01-01T12:00:00" ;
    :StopTime = "2012-12-31T00:00:00" ;
    :geospatial_lat_min = 50.9974f ;
    :geospatial_lat_max = 55.6386f ;
    :geospatial_lon_min = -1.10761f ;
    :geospatial_lon_max = 8.75949f ;
    :licence = "Creative Commons Attribution Share-Alike (CC-by-SA) 4.0 unported" ;
    :distribution_statement = "Data can be used, reused and distributed under the CC-by-SA" ;
    :history = "Compiled for WDCC 2024-08-27" ;
}
EOT

ncgen -o meta.nc meta.cdl
