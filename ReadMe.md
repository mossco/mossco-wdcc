<!--
SPDX-License-Identifier: CC0-1.0
SPDX-FileCopyrightText: 2024 Helmholtz-Zentrum hereon GmbH
SPDX-FileContributor: Carsten Lemmen <carsten.lemmen@hereon.de>
-->

# MOSSCO WDCC deployment

This repository contains files for deploying MOSSCO results to WDCC.

# Installation

Prerequisites are

1. NetCDF binaries (`ncgen`)
2. NetCDF operators (`ncks`)
3. CEDA CF-Checker (`cfchecks`)
4. Python with `numpy` and `netCDF4`

You can install those dependencies yourself in your preferred environment, or use `conda` with the provided `environment.yaml` by issuing the command `conda env create --file environment.yaml`.

## Adjusting to a new simulation

1. Edit the list of variables to process in `./stitch_variables.sh`
2. Edit the metadata in `create_meta.sh`

## Workflow

1. Create a new directory ${WORK}
2. Link your MOSSCO original output files into this directory
3. Run `./stitch_variables.sh`; this will create `slurm` jobs and individual `run` scripts for all variables.
4. Run `sbatch slurm*` or create an interactive session to execute `run*sh`
5. The resulting stitched files will be named `stitch_<variable>.nc`
6. Run `create_meta.sh` to generate `meta.nc`
7. Run `create_soil.py` to generate `soil.nc`
8. Run `create_globals.sh` to generate global files for PET, time variables and cell area.
9. Run `add_meta.sh`
10. Synchronize files to Levante
