#! /bin/bash
#!/bin/bash
# SPDX-License-Identifier: CC0-1.0
# SPDX-FileCopyrightText: 2024 Helmholtz-Zentrum hereon GmbH
# SPDX-FileContributor: Carsten Lemmen <carsten.lemmen@hereon.de>
#

variables=(
Chl_chl_in_water
Chl_chl_z_velocity_in_water
Detritus_Carbon_detC_in_water
Detritus_Carbon_detC_z_velocity_in_water
Detritus_Nitrogen_detN_in_water
Detritus_Nitrogen_detN_z_velocity_in_water
Detritus_Phosphorus_detP_in_water
Detritus_Phosphorus_detP_z_velocity_in_water
Dissolved_Inorganic_Nitrogen_DIN_nutN_in_water
Dissolved_Inorganic_Nitrogen_DIN_nutN_z_velocity_in_water
Dissolved_Inorganic_Phosphorus_DIP_nutP_in_water
Dissolved_Inorganic_Phosphorus_DIP_nutP_z_velocity_in_water
Dissolved_Organic_Carbon_domC_in_water
Dissolved_Organic_Carbon_domC_z_velocity_in_water
Dissolved_Organic_Nitrogen_domN_in_water
Dissolved_Organic_Nitrogen_domN_z_velocity_in_water
Dissolved_Organic_Phosphorus_domP_in_water
Dissolved_Organic_Phosphorus_domP_z_velocity_in_water
Phytplankton_Carbon_phyC_in_water
Phytplankton_Carbon_phyC_z_velocity_in_water
Phytplankton_Nitrogen_phyN_in_water
Phytplankton_Nitrogen_phyN_z_velocity_in_water
Phytplankton_Phosphorus_phyP_in_water
Phytplankton_Phosphorus_phyP_z_velocity_in_water
Virus_C_density_in_cells_vir_in_water
Virus_C_density_in_cells_vir_z_velocity_in_water
Zooplankton_Carbon_zooC_in_water
Zooplankton_Carbon_zooC_z_velocity_in_water
_datt_in_water
_vphys_in_water
buoyancy_frequency_squared_in_water
denitrification_rate_in_soil
depth_averaged_x_velocity_in_water
depth_averaged_y_velocity_in_water
detritus-P_in_soil
detritus-P_upward_flux_at_soil_surface
dissipation_of_tke_at_soil_surface
dissipation_of_tke_in_water
dissolved_oxygen_in_soil
dissolved_oxygen_upward_flux_at_soil_surface
dissolved_reduced_substances_in_soil
dissolved_reduced_substances_upward_flux_at_soil_surface
fast_detritus_C_in_soil
fast_detritus_C_upward_flux_at_soil_surface
fraction_of_Rubisco_Rub_in_water
fraction_of_Rubisco_Rub_z_velocity_in_water
layer_center_depth_in_soil
layer_height_at_soil_surface
layer_height_in_soil
layer_height_in_water
maximum_bottom_stress
mole_concentration_of_ammonium_in_soil
mole_concentration_of_ammonium_upward_flux_at_soil_surface
mole_concentration_of_nitrate_in_soil
mole_concentration_of_nitrate_upward_flux_at_soil_surface
mole_concentration_of_phosphate_in_soil
mole_concentration_of_phosphate_upward_flux_at_soil_surface
pPads_in_water
phosphate_adsorption_in_soil
photosynthetically_available_radiation_in_soil
porosity_in_soil
practical_salinity_in_water
result_in_water
shear_frequency_squared_in_water
slow_detritus_C_in_soil
slow_detritus_C_upward_flux_at_soil_surface
surface_downwelling_photosynthetic_radiative_flux
temperature_at_soil_surface
temperature_in_soil
temperature_in_water
turbulent_diffusivity_of_heat_in_water
turbulent_diffusivity_of_momentum_at_soil_surface
turbulent_diffusivity_of_momentum_in_water
turbulent_kinetic_energy_at_soil_surface
turbulent_kinetic_energy_in_water
water_depth_at_soil_surface
wind_x_velocity_at_10m
wind_y_velocity_at_10m
x_velocity_at_soil_surface
x_velocity_in_water
y_velocity_at_soil_surface
y_velocity_in_water
)

for var in "${variables[@]}"; do
cat << EOT > slurm_${var}.sh
#!/bin/bash -x
#SBATCH --ntasks=1
#SBATCH --output=${var}.stdout
#SBATCH --error=${var}.stderr
#SBATCH --time=2:00:00
#SBATCH --mail-user=carsten.lemmen@hereon.de
#SBATCH --mail-type=NONE
#SBATCH --job-name=${var}
#SBATCH --partition=pRush
#export OMP_NUM_THREADS=1

test -f ${var}.stdout && rm -f ${var}.stdout
test -f ${var}.stderr && rm -f ${var}.stderr

echo "Loading modules ..."
module load compilers/intel
module load netcdf
module load hdf5
module load applications/python/3.8

echo "Starting stitch"
srun --propagate=STACK --mpi=pmi2 ./run_${var}.sh
EOT

cat << EOT > run_${var}.sh
#!/bin/bash -x
/gpfs/home/lemmen/opt/bin/stitch mossco_gfbfrr --include=${var} --out=stitch_${var}.nc > log_${var}.log
EOT
chmod +x ./run_${var}.sh

done

var=water_column_area
cat << EOT > run_${var}.sh
#!/bin/bash -x
/gpfs/home/lemmen/opt/bin/stitch mossco_gfbfrr --include=${var} --out=stitch_${var}.nc > log_${var}.log
ncks -O -d time,0 -x -v "time,getmGrid.*" -C stitch_water_column_area.nc temp_water_column_area.nc
ncwa -O -a time temp_water_column_area.nc global_water_column_area.nc
ncrename -v pelagic_horizontal_grid_y,getmGrid2D_y  global_water_column_area.nc
ncrename -v pelagic_horizontal_grid_x,getmGrid2D_x  global_water_column_area.nc
ncrename -d pelagic_horizontal_grid_1,getmGrid2D_1  global_water_column_area.nc
ncrename -d pelagic_horizontal_grid_2,getmGrid2D_2  global_water_column_area.nc
ncatted -h -a coordinates,water_column_area,o,c,"getmGrid2D_y getmGrid2D_x" global_water_column_area.nc
EOT
chmod +x ./run_${var}.sh

var=pet_getmGrid2D
cat << EOT > run_${var}.sh
#!/bin/bash -x
/gpfs/home/lemmen/opt/bin/stitch mossco_gfbfrr --include=${var} --out=global_${var}.nc > log_${var}.log
EOT
chmod +x ./run_${var}.sh

var=time
cat << EOT > run_${var}.sh
#!/bin/bash -x
ncks -O -v time,wallclock_time,elapsed_wallclock_time,speedup,doy mossco_gfbfrr.41.nc global_time.nc > log_${var}.log
EOT
chmod +x ./run_${var}.sh

# salloc -t 240 -n 10 -p pRush
