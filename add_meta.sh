#!/bin/bash
# SPDX-License-Identifier: CC0-1.0
# SPDX-FileCopyrightText: 2024 Helmholtz-Zentrum hereon GmbH
# SPDX-FileContributor: Carsten Lemmen <carsten.lemmen@hereon.de>
#

mkdir -p physical pelagic benthic
rm -f add_meta.log

for G in stitch_*nc; do

  B=${G##stitch_}   # base name with .nc
  V=${B%%.nc}      # variable name
  F=mossco_${B}

  # Remove upper case integer indices of grid
  # not compliant with CF, don't copy 3D info to 2D files
  echo "$G --> $F .."
  case "$V" in
    *in_water)
    ncks -O -x -v ".*_X,.*_Y,pelagic_horizontal_grid.*" $G $F ;;
    *)
    ncks -O -x -v ".*_X,.*_Y,pelagic_horizontal_grid.*,getmGrid3D.*" $G $F ;;
  esac

  ncks -A -v pet_getmGrid2D global_pet_getmGrid2D.nc $F
  ncks -A -v water_column_area global_water_column_area.nc $F
  ncks -A -v time,doy,elapsed_wallclock_time,wallclock_time,speedup global_time.nc $F
  ncks -A soil.nc $F
  ncks -A meta.nc $F

  ncatted -h -a units,wallclock_time,o,c,"seconds since 1970-01-01 00:00:00" $F
  ncatted -h -a standard_name,wallclock_time,o,c,"time" $F
  ncatted -h -a long_name,wallclock_time,o,c,"wallclock time" $F

  ncatted -h -a units,elapsed_wallclock_time,o,c,"seconds since 2017-12-13T20:11:51" $F
  ncatted -h -a standard_name,elapsed_wallclock_time,o,c,"time" $F
  ncatted -h -a long_name,elapsed_wallclock_time,o,c,"elapsed wallclock time" $F

  ncatted -h -a long_name,time,o,c,"simulation time" $F
  ncatted -h -a calendar,time,o,c,"standard" $F
  ncatted -h -a long_name,doy,o,c,"day of year" $F

  ncatted -h -a standard_name,speedup,o,c,"ratio_of_time_to_time" $F
  ncatted -h -a long_name,speedup,o,c,"speedup as simulation time over elapsed wallclock time" $F

  ncatted -h -a standard_name,water_column_area,o,c,"cell_area" $F
  ncatted -h -a cell_methods,water_column_area,d,c,"" $F

  ncatted -h -a coordinates,pet_getmGrid2D,o,c,"getmGrid2D_y getmGrid2D_x" $F
  ncatted -h -a coordinates,water_column_area,o,c,"getmGrid2D_y getmGrid2D_x" $F

  ncatted -h -a standard_name,getmGrid.D_x,o,c,"longitude" $F
  ncatted -h -a standard_name,getmGrid.D_y,o,c,"latitude" $F
  ncatted -h -a units,getmGrid.D_x,o,c,"degree_east" $F
  ncatted -h -a units,getmGrid.D_y,o,c,"degree_north" $F

  case "$V" in
    *in_water)
    ncatted -h -a standard_name,getmGrid3D_z,o,c,"height_above_mean_sea_level" $F
    ;;
    *in_soil)
    ncatted -h -a coordinates,.*_in_soil,o,c,"level_center_depth_in_soil getmGrid2D_y getmGrid2D_x" $F ;
    ;;
    *)
    ;;
  esac

  case "$V" in
    denitrification_rate_in_soil)
    ncatted -h -a units,$V,o,c,"mmol m-3 s-1" $F ;
    ncatted -h -a description,$V,o,c,"Rate of denitrification" $F ;
    ncatted -h -a long_name,$V,o,c,"tendency_of_mole_concentration_of_nitrogen_in_soil" $F ;;
    mole_concentration_of_nitrate_in_soil)
    ncatted -h -a units,$V,o,c,"mmol m-3" $F ;
    ncatted -h -a description,$V,o,c,"Concentration of dissolved nitrate" $F ;
    ncatted -h -a long_name,$V,o,c,"mole_concentration_of_nitrate_in_soil" $F ;;
    Dissolved_Inorganic_Nitrogen_DIN_nutN_in_water)
    ncatted -h -a units,$V,o,c,"mmol m-3" $F ;
    ncatted -h -a description,$V,o,c,"Total dissolved inorganic nitrogen" $F ;
    ncatted -h -a standard_name,$V,o,c,"mole_concentration_of_dissolved_inorganic_nitrogen_in_sea_water" $F ;;
    Dissolved_Inorganic_Carbon_DIC_nutC_in_water)
    ncatted -h -a units,$V,o,c,"mmol m-3" $F ;
    ncatted -h -a description,$V,o,c,"Total dissolved inorganic carbon" $F ;
    ncatted -h -a standard_name,$V,o,c,"mole_concentration_of_dissolved_inorganic_carbon_in_sea_water" $F ;;
    Dissolved_Inorganic_Phosphorus_DIP_nutP_in_water)
    ncatted -h -a units,$V,o,c,"mmol m-3" $F ;
    ncatted -h -a description,$V,o,c,"Total dissolved inorganic phosphorus" $F ;
    ncatted -h -a standard_name,$V,o,c,"mole_concentration_of_dissolved_inorganic_phosphorus_in_sea_water" $F
    ;;
    Dissolved_Organic_Nitrogen_domN_in_water)
    ncatted -h -a units,$V,o,c,"mmol m-3" $F ;
    ncatted -h -a description,$V,o,c,"Total dissolved organic nitrogen" $F ;
    ncatted -h -a standard_name,$V,o,c,"mole_concentration_of_dissolved_organic_nitrogen_in_sea_water" $F ;;
    Dissolved_Organic_Phosphorus_domP_in_water)
    ncatted -h -a units,$V,o,c,"mmol m-3" $F ;
    ncatted -h -a description,$V,o,c,"Total dissolved organic phosphorus" $F ;
    ncatted -h -a standard_name,$V,o,c,"mole_concentration_of_dissolved_organic_phosphorus_in_sea_water" $F ;;
    Dissolved_Organic_Carbon_domC_in_water)
    ncatted -h -a units,$V,o,c,"mmol m-3" $F ;
    ncatted -h -a description,$V,o,c,"Total dissolved organic carbon" $F ;
    ncatted -h -a standard_name,$V,o,c,"mole_concentration_of_dissolved_organic_carbon_in_sea_water" $F ;;
    Phytplankton_Nitrogen_phyN_in_water)
    ncatted -h -a units,$V,o,c,"mmol m-3" $F ;
    ncatted -h -a description,$V,o,c,"Phytoplankton-bound total nitrogen" $F ;
    ncatted -h -a standard_name,$V,o,c,"mole_concentration_of_biological_taxon_expressed_as_nitrogen_in_sea_water" $F ;;
    Phytplankton_Carbon_phyC_in_water)
    ncatted -h -a units,$V,o,c,"mmol m-3" $F ;
    ncatted -h -a description,$V,o,c,"Phytoplankton-bound total carbon" $F ;
    ncatted -h -a standard_name,$V,o,c,"mole_concentration_of_biological_taxon_expressed_as_carbon_in_sea_water" $F ;;
    Phytplankton_Phosphorus_phyP_in_water)
    ncatted -h -a units,$V,o,c,"mmol m-3" $F ;
    ncatted -h -a description,$V,o,c,"Phytoplankton-bound total phosphorus" $F ;
    ncatted -h -a long_name,$V,o,c,"mole_concentration_of_biological_taxon_expressed_as_phosphorus_in_sea_water" $F ;;
    Virus_C_density_in_cells_vir_in_water|_vphys_in_water|fraction_of_Rubisco_Rub_in_water)
    ncatted -h -a description,$V,o,c,"Fraction of viral load" $F ;
    ncatted -h -a units,$V,o,c,"1" $F ;;
    pPads_in_water)
    ncatted -h -a units,$V,o,c,"1" $F ;;
    layer_height_in_water)
    ncatted -h -a description,$V,o,c,"Thickness (height) of model layer" $F ;
    ncatted -h -a standard_name,layer_height_in_water,o,c,"cell_thickness" $F ;;
    Chl_chl_in_water)
    ncatted -h -a units,Chl_chl_in_water,o,c,"mg m-3" $F ;
    ncatted -h -a description,$V,o,c,"Phytoplankton-bound chlorophyll" $F ;
    ncatted -h -a standard_name,Chl_chl_in_water,o,c,"mass_concentration_of_biological_taxon_expressed_as_chlorophyll_in_sea_water" $F ;
    ;;
    Detritus_Nitrogen_detN_in_water)
    ncatted -h -a units,$V,o,c,"mmol m-3" $F ;
    ncatted -h -a description,$V,o,c,"Total nitrogen in detritus" $F ;
    ncatted -h -a standard_name,$V,o,c,"mole_concentration_of_organic_detritus_expressed_as_nitrogen_in_sea_water" $F ;;
    Detritus_Phosphorus_detP_in_water)
    ncatted -h -a units,$V,o,c,"mmol m-3" $F ;
    ncatted -h -a description,$V,o,c,"Total phosphorus in detritus" $F ;
    ncatted -h -a long_name,$V,o,c,"mole_concentration_of_organic_detritus_expressed_as_phosphorus_in_sea_water" $F ;;
    Detritus_Carbon_detC_in_water)
    ncatted -h -a units,$V,o,c,"mmol m-3" $F ;
    ncatted -h -a description,$V,o,c,"Total carbon in detritus" $F ;
    ncatted -h -a standard_name,$V,o,c,"mole_concentration_of_organic_detritus_expressed_as_carbon_in_sea_water" $F ;;
    detritus-P_in_soil)
    ncatted -h -a units,$V,o,c,"mmol m-3" $F ;
    ncatted -h -a description,$V,o,c,"Total phosphorus in detritus" $F ;
    ncatted -h -a long_name,$V,o,c,"mole_concentration_of_organic_detritus_expressed_as_phosphorus_in_soil" $F ;;
    detritus-P_upward_flux_at_soil_surface)
    ncatted -h -a units,$V,o,c,"mmol m-2 s-1" $F ;
    ncatted -h -a description,$V,o,c,"Total detrital phosphorus upward flux" $F ;
    ncatted -h -a long_name,$V,o,c,"upward_flux_of_mole_concentration_of_organic_detritus_expressed_as_phosphorus_at_soil_surface" $F ;;
    mole_concentration_of_nitrate_upward_flux_at_soil_surface)
    ncatted -h -a units,$V,o,c,"mmol m-2 s-1" $F ;
    ncatted -h -a description,$V,o,c,"Dissolved nitrate upward flux" $F ;
    ncatted -h -a long_name,$V,o,c,"upward_flux_of_mole_concentration_of_organic_detritus_expressed_as_phosphorus_at_soil_surface" $F  ;;
    dissolved_oxygen_upward_flux_at_soil_surface)
    ncatted -h -a units,$V,o,c,"mmol m-2 s-1" $F ;
    ncatted -h -a description,$V,o,c,"Dissolved molecular oxygen upward flux" $F ;
    ncatted -h -a long_name,$V,o,c,"upward_flux_of_mole_concentration_of_dissolved_oxygen_at_soil_surface" $F  ;;
    dissolved_oxygen_in_soil)
    ncatted -h -a units,$V,o,c,"mmol m-3" $F ;
    ncatted -h -a description,$V,o,c,"Dissolved molecular oxgen" $F ;
    ncatted -h -a long_name,$V,o,c,"mole_concentration_of_dissolved_molecular_oxygen_in_soil" $F  ;;
    fast_detritus_C_in_soil)
    ncatted -h -a units,$V,o,c,"mmol m-3" $F ;
    ncatted -h -a description,$V,o,c,"Labile (fresh) detritus carbon" $F ;
    ncatted -h -a long_name,$V,o,c,"mole_concentration_of_organic_detritus_expressed_as_labile_carbon_in_soil" $F  ;;
    slow_detritus_C_in_soil)
    ncatted -h -a units,$V,o,c,"mmol m-3" $F ;
    ncatted -h -a description,$V,o,c,"Semilabile (older) detritus carbon" $F ;
    ncatted -h -a long_name,$V,o,c,"mole_concentration_of_organic_detritus_expressed_as_semilabile_carbon_in_soil" $F  ;;
    phosphate_adsorption_in_soil)
    ncatted -h -a units,$V,o,c,"mmol m-3" $F ;
    ncatted -h -a long_name,$V,o,c,"mole_concentration_of_absorbed_phosphorus_in_soil" $F  ;;
    fast_detritus_C_upward_flux_at_soil_surface)
    ncatted -h -a units,$V,o,c,"mmol m-2 s-1" $F ;
    ncatted -h -a description,$V,o,c,"Labile (fresh) detritus carbon upward flux" $F ;
    ncatted -h -a long_name,$V,o,c,"upward_flux_of_mole_concentration_of_organic_detritus_expressed_as_labile_carbon_in_soil" $F  ;;
    slow_detritus_C_upward_flux_at_soil_surface)
    ncatted -h -a units,$V,o,c,"mmol m-2 s-1" $F ;
    ncatted -h -a description,$V,o,c,"Semilabile (older) detritus carbon upward flux" $F ;
    ncatted -h -a long_name,$V,o,c,"upward_flux_of_mole_concentration_of_organic_detritus_expressed_as_semilabile_carbon_in_soil" $F  ;;
    layer_center_depth_in_soil)
    ncatted -h -a description,$V,o,c,"Central depth of model layer" $F ;
    ncatted -h -a standard_name,$V,o,c,"depth_below_sea_floor" $F  ;;
    water_depth_at_soil_surface)
    ncatted -h -a description,$V,o,c,"Total water depth" $F ;
    ncatted -h -a standard_name,$V,o,c,"water_table_depth" $F  ;;
    *)
    ;;
  esac

  ncatted -h -a history_of_appended_files,global,d,c,"" $F
  ncatted -a history,global,o,c,"Original tile files stitched and separated by variable. Metadata added for WDCC" $F

  test -x cfchecks && cfchecks $F >> add_meta.log

#  case "$V" in
#   *tke*|*buoy*|*velocity*|*turbulent*|*salinity*|*temperature*|*stress*)
#      mv $F physical/
#      ;;
#    *soil*)
#      mv $F benthic/
#      ;;
#    *)
#      mv $F pelagic/
#      ;;
#  esac
done
